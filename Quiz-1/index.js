// Soal quiz no 1

function jumlah_kata(fungsi) {
    var kata = fungsi.split(" ");
    return kata.length
}

var kalimat_1 = "Halo nama saya Muhammad Iqbal Mubarok"
var kalimat_2 = "Saya Iqbal"


console.log(jumlah_kata(kalimat_1))
console.log(jumlah_kata(kalimat_2))

// Soal quiz no 2

function next_date(tanggal, bulan, tahun) {
    if (tanggal == 29 && bulan == 2 && tahun == 2020) {
        return tanggal - 28 + " Maret "+ tahun
    }else if (tanggal == 28 && bulan == 2 && tahun == 2021) {
        return tanggal - 27 + " Maret "+ tahun
    }else if (tanggal == 31 && bulan == 12 && tahun == 2020) {
        // return tanggal - 30 + " Januari "+ tahun + 1
        return tanggal - 30 + " Januari 2021"
    }
    switch(bulan) {
        case 1: { console.log( ' Januari '); break; }
        case 2: { console.log( ' Februari '); break; }
        case 3: { console.log( ' Maret '); break; }
        case 4: { console.log( ' April '); break; }
        case 5: { console.log( ' Mei '); break; }
        case 6: { console.log( ' Juni '); break; }
        case 7: { console.log( ' Juli '); break; }
        case 8: { console.log( ' Agustus '); break; }
        case 9: { console.log( ' September '); break; }
        case 10: { console.log( ' Oktober '); break; }
        case 11: { console.log( ' November '); break; }
        case 12: { console.log( ' Desember '); break; }
        default: { console.log( 'nihil'); }
    }
}

var tanggal = 29
var bulan = 2
var tahun = 2020

console.log(next_date(tanggal , bulan , tahun ))

var tanggal = 28
var bulan = 2
var tahun = 2021

console.log(next_date(tanggal , bulan , tahun ))

var tanggal = 31
var bulan = 12
var tahun = 2020

console.log(next_date(tanggal , bulan , tahun ))