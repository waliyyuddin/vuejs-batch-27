// Soal No 1

// membuat luas dan keliling persegi panjang menggunakan arrow function serta dengan let + const

const luasPersegiPanjang = (a,b) => {
    let panjang = a;
    let lebar = b;
    return panjang * lebar
}

console.log(luasPersegiPanjang(5,2))
console.log(" ")

const kelilingPersegiPanjang = (lebarKiriKanan, panjangKiriKanan) => {
    return lebarKiriKanan + panjangKiriKanan + lebarKiriKanan + panjangKiriKanan
}

console.log(kelilingPersegiPanjang(5,2))
console.log(" ")

// Soal No 2
// ubah kode di bawah ke dalam arrow function dan object literal es6 yang lebih sederhana

// opsi 1 atau cara 1
const newFunction = (firstName, lastName) => {
    const awal = firstName
    const akhir = lastName
    const fullName = {awal, akhir} 
      return awal + " " + akhir
}
   
    //Driver Code 
console.log(newFunction("William", "Imoh"))

// opsi 2 atau cara 2
const newFunction2 = (firstName, lastName) => {
    const awal = firstName
    const akhir = lastName
    const namaPanjang = awal + " " + akhir
    const fullName = {namaPanjang} 
    return fullName
}
    
    //Driver Code 
console.log(newFunction2("William", "Imoh"))
console.log(" ")

// Soal No 3

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

// es5
// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const address = newObject.address;
// const hobby = newObject.hobby;

// penggunaan destructuring dalam es6
// Driver code
const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby)
console.log(" ")

// Soal No 4
// kombinasikan array di bawah menggunakan spreading es6

let west = ["Will", "Chris", "Sam", "Holly"]
let east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
let combined = [...west, ...east]

//Driver Code
console.log(combined)
console.log(" ")

// Soal No 5
// sederhanakan string d bawah menggunakan template literals ES6

const planet = "earth" 
const view = "glass" 
// var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 

// penggunaan template ES6
const hasil = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log(hasil)