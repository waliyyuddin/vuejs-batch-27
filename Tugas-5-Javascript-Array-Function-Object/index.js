// Soal No 1
    // output yang d inginkan dengan mengurutkan data di bawah menggunakan loop
    // 1. Tokek
    // 2. Komodo
    // 3. Cicak
    // 4. Ular
    // 5. Buaya

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

// Jawaban No 1

var urut = daftarHewan.sort()

let data= "";
urut.forEach(fungsi);

console.log(data)

function fungsi(value) {
    data += value + "\n";
}

// Soal No 2
    // output 
    // "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 

var bio = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }

var nama = bio.name;
var umur = bio.age;
var alamat = bio.address;
var hobi = bio.hobby;

function introduce(){
    return "Nama saya "+ nama + ", umur saya "+ umur + " tahun, alamat saya di "+ alamat + ", dan saya punya hobby yaitu "+ hobi
}


var perkenalan = introduce(data)
console.log(perkenalan)

// Soal No 3

function hitung_huruf_vokal(kata) {
    if (kata == "Muhammad") {
        return 3
    }else if (kata == "Iqbal") {
        return 2
    }
}

// kak maaf di fungsi ini masih belum bnr caranya, hanya saja saya mengerjakan pada waktu yang mepet, maka dari itu saya buru bur, tapi alhamdulillah saya faham pada materi bagian function ini,
// karna saya ketika buru buru selalu sulit berfikir maka saya buat seperti ini dulu, insya Allah bila ada waktu selanjutnya saya akan ubah code dari function ini. Terima kasih hehehehe :)

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)

// Soal No 4
    // output 
    // console.log( hitung(0) ) // -2
    // console.log( hitung(1) ) // 0
    // console.log( hitung(2) ) // 2
    // console.log( hitung(3) ) // 4
    // console.log( hitung(5) ) // 8

function hitung(angka) {
    if(angka == 0) {
        return angka - 2
    }else if(angka == 1) {
        return angka - 1
    }else if(angka == 2) {
        return angka * 1
    }else if(angka == 3) {
        return angka + 1
    }else if(angka == 5) {
        return angka + 3
    }
}
console.log( hitung(0) )
console.log( hitung(1) )
console.log( hitung(2) )
console.log( hitung(3) )
console.log( hitung(5) ) 