// Soal No 1
    // Output "saya senang belajar JAVASCRIPT"

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

// Jawaban
var kataSatu = pertama.substring(0, 4)
var kataDua = pertama.substring(11, 19)
var kataTiga = kedua.substring(0, 7)
var kataEmpat = kedua.substring(7, 18)

var capital = kataEmpat.toUpperCase(kataEmpat)

var gabung1 = kataSatu.concat(kataDua)
var gabung2 = kataTiga.concat(capital)

var isi = gabung1.concat(gabung2)

// isi
console.log(isi);


// Soal No 2
    // Output 24 (integer).

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

// Jawaban
var int1 = parseInt(kataPertama)
var int2 = parseInt(kataKedua)
var int3 = parseInt(kataKetiga)
var int4 = parseInt(kataKeempat)

// operasi
var hasil = int1 * int2 + int3

// isi
console.log(hasil)


// Soal No 3
    // Output  Kata Pertama: wah
            // Kata Kedua: javascript
            // Kata Ketiga: itu
            // Kata Keempat: keren
            // Kata Kelima: sekali

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 

// Jawaban
var kataKedua = kalimat.substring(4, 14); // do your own! 
var kataKetiga = kalimat.substring(15, 18); // do your own! 
var kataKeempat = kalimat.substring(19, 24); // do your own! 
var kataKelima = kalimat.substring(25, 31); // do your own! 

// isi
console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);