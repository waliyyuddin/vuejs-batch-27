// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Tulis code untuk memanggil function readBooks di sini

// const satu = function cara1(callback){
//     console.log('mulai')
//     callback();
//     console.log('selesai')
// }

// const dua = function cara2(){
//     setTimeout(() => {
//         console.log("ini isi")
//     }, 2000)
// }

// satu(dua)

// const baca = () => {
//     const satu = () => {
//         const buku1 = books[0]
//         return buku1
//     }
//     const dua = () => books[1]
//     const tiga = () => books[2]
//     const empat = () => books[3]
//     satu(function (sisaBuku1){
//         dua(sisaBuku1, function(sisaBuku2){
//             tiga(sisaBuku2, function(sisaBuku3){
//                 empat(sisaBuku3, function(sisaBuku4){
//                     console.log(sisaBuku4)
//                 })
//             })
//         })
//     })
// }

// readBooks(10000, books, baca)
// readBooks(10000, books[0], baca)
// readBooks(10000, books[1], baca)
// readBooks(10000, books[2], baca)
// readBooks(10000, books[3], baca)

const time = 10000

readBooks(time, books[0], (sisaWaktu0) => {
    readBooks(sisaWaktu0, books[1], (sisaWaktu1) => {
        readBooks(sisaWaktu1, books[2], (sisaWaktu2) => {
            readBooks(sisaWaktu2, books[3], (sisaWaktu3) => {
                console.log(sisaWaktu3)
            })
        })
    })
})