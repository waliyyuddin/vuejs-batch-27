var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 

// const baca2 = () => {
//     setTimeout (() => {

//     }, 10000)
// }

// baca2(readBooksPromise)

const time = 10000

readBooksPromise(time, books[0])
    .then( (data) => {
        return readBooksPromise(data.sisaWaktu, books[1])
    })
    .then( (data) => {
        return readBooksPromise(data.sisaWaktu, books[2])
    })
    .then( (data) => {
        return readBooksPromise(data.sisaWaktu, books[3])
    })

// const baca = async () => {
//     try {
//         const data1 = await readBooksPromise(time, books[0])
//         const data2 = await readBooksPromise(data1.sisaWaktu, books[1])
//         const data3 = await readBooksPromise(data2.sisaWaktu, books[2])
//         const data4 = await readBooksPromise(data3.sisaWaktu, books[3])

//         console.log(data4)
        
//     } catch (data) {
//         console.log(`saya sudah tidak punya waktu untuk baca ${book.name}`)
//     }
// }

// baca()

// const read = async () => {
//     const data1 = await readBooksPromise(time, books[0])
//     const data2 = await readBooksPromise(data1.sisaWaktu, books[1])
//     const data3 = await readBooksPromise(data2.sisaWaktu, books[2])
//     const data4 = await readBooksPromise(data3.sisaWaktu, books[3])

//     console.log(data4)

// }

// read()