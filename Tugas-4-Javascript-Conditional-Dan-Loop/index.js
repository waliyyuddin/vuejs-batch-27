// Soal No 1

var nilai = 90;

// condition from task (kondisi dari tugas)

// nilai >= 85 indeksnya A
// nilai >= 75 dan nilai < 85 indeksnya B
// nilai >= 65 dan nilai < 75 indeksnya c
// nilai >= 55 dan nilai < 65 indeksnya D
// nilai < 55 indeksnya E

// Jawaban soal no 1

if (nilai >= 85) {
    console.log("A")
}else if (nilai >= 75 && nilai < 85) {
    console.log("B")
}else if (nilai >= 65 && nilai < 75) {
    console.log("C")
}else if (nilai >= 55 && nilai < 65) {
    console.log("D")
}else if (nilai < 55) {
    console.log("E")
}

console.log(" ")

// Soal No 2
    // output untuk switch case = 22 Juli 2020 (sesuai tanggal lahir masing-masing) {18 Desember 2001}

var tanggal = 18;
var bulan = 12;
var tahun = 2001;

// Jawaban soal no 2

switch(bulan) {
    case 1: { console.log(tanggal + ' Januari ' + tahun); break; }
    case 2: { console.log(tanggal + ' Februari ' + tahun); break; }
    case 3: { console.log(tanggal + ' Maret ' + tahun); break; }
    case 4: { console.log(tanggal + ' April ' + tahun); break; }
    case 5: { console.log(tanggal + ' Mei ' + tahun); break; }
    case 6: { console.log(tanggal + ' Juni ' + tahun); break; }
    case 7: { console.log(tanggal + ' Juli ' + tahun); break; }
    case 8: { console.log(tanggal + ' Agustus ' + tahun); break; }
    case 9: { console.log(tanggal + ' September ' + tahun); break; }
    case 10: { console.log(tanggal + ' Oktober ' + tahun); break; }
    case 11: { console.log(tanggal + ' November ' + tahun); break; }
    case 12: { console.log(tanggal + ' Desember ' + tahun); break; }
    default: { console.log('Belum lahir'); }
}
console.log(" ")

// Soal No 3
    // output yang di minta (boleh menggunakan while, for, do while)
    // Output untuk n=3 :

    // #
    // ##
    // ###
    
    // Output untuk n=7 :

    // #
    // ##
    // ###
    // ####
    // #####
    // ######
    // #######

// Jawaban soal no 3

console.log("n=3")
console.log(" ")

function segitiga(baris) {
    var pola = '';

    for (var i = 1; i <= baris; i++) {
        for (var j = 1; j <= i; j++) {
            pola += "#";
        }
        pola += "\n";
    }
    return pola;
}
console.log(segitiga(3))

console.log(" ")

console.log("n=7")
console.log(" ")

function segitiga(baris) {
    var pola = '';

    for (var i = 1; i <= baris; i++) {
        for (var j = 1; j <= i; j++) {
            pola += "#";
        }
        pola += "\n";
    }
    return pola;
}
console.log(segitiga(7))

console.log(" ")

// Soal No 4
    // output yang di harapkan 
    // Output untuk m = 3

    // 1 - I love programming
    // 2 - I love Javascript
    // 3 - I love VueJS
    // ===

    // Output untuk m = 5

    // 1 - I love programming
    // 2 - I love Javascript
    // 3 - I love VueJS
    // ===
    // 4 - I love programming
    // 5 - I love Javascript

    // Output untuk m = 7

    // 1 - I love programming
    // 2 - I love Javascript
    // 3 - I love VueJS
    // ===
    // 4 - I love programming
    // 5 - I love Javascript
    // 6 - I love VueJS
    // ======
    // 7 - I love programming


    // Output untuk m = 10

    // 1 - I love programming
    // 2 - I love Javascript
    // 3 - I love VueJS
    // ===
    // 4 - I love programming
    // 5 - I love Javascript
    // 6 - I love VueJS
    // ======
    // 7 - I love programming
    // 8 - I love Javascript
    // 9 - I love VueJS
    // =========
    // 10 - I love programming

// Jawaban soal no 4

console.log("m = 3")
console.log(" ")

for (var a = 1; a <= 3; a++) {
    if (a % 2 == 0) {
        console.log(a + ' - I love Javascript');
    }else if (a % 3 == 0) {
        console.log(a + ' - I love VueJS');
    }else {
        console.log(a + ' - I love programming');
    }
}

console.log("===")
console.log(" ")

console.log("m = 5")
console.log(" ")

for (var b = 1; b <= 3; b++) {
    if (b % 2 == 0) {
        console.log(b + ' - I love Javascript');
    }else if (b % 3 == 0) {
        console.log(b + ' - I love VueJS');
    }else {
        console.log(b + ' - I love programming');
    }
}

console.log("===")

for (var b = 4; b <= 5; b++) {
    if (b % 2 == 0) {
        console.log(b + ' - I love programming');
    }else if (b % 3 == 0) {
        console.log(b + ' - I love VueJS');
    }else {
        console.log(b + ' - I love Javascript');
    }
}
console.log(" ")

console.log("m = 7")
console.log(" ")

for (var c = 1; c <= 3; c++) {
    if (c % 2 == 0) {
        console.log(c + ' - I love Javascript');
    }else if (c % 3 == 0) {
        console.log(c + ' - I love VueJS');
    }else {
        console.log(c + ' - I love programming');
    }
}

console.log("===")

for (var c = 4; c <= 6; c++) {
    if (c % 4 == 0) {
        console.log(c + ' - I love programming');
    }else if (c % 6 == 0) {
        console.log(c + ' - I love VueJS');
    }else {
        console.log(c + ' - I love Javascript');
    }
}

console.log("=====")

for (var c = 7; c <= 7; c++) {
    if (c % 2 == 0) {
        console.log(c + ' - I love Javascript');
    }else if (c % 3 == 0) {
        console.log(c + ' - I love VueJS');
    }else {
        console.log(c + ' - I love programming');
    }
}

console.log(" ")

console.log("m = 10")
console.log(" ")

for (var b = 1; b <= 3; b++) {
    if (b % 2 == 0) {
        console.log(b + ' - I love Javascript');
    }else if (b % 3 == 0) {
        console.log(b + ' - I love VueJS');
    }else {
        console.log(b + ' - I love programming');
    }
}

console.log("===")
console.log(" ")

for (var b = 4; b <= 6; b++) {
    if (b % 4 == 0) {
        console.log(b + ' - I love programming');
    }else if (b % 6 == 0) {
        console.log(b + ' - I love VueJS');
    }else {
        console.log(b + ' - I love Javascript');
    }
}

console.log("=====")

for (var b = 7; b <= 9; b++) {
    if (b % 2 == 0) {
        console.log(b + ' - I love Javascript');
    }else if (b % 3 == 0) {
        console.log(b + ' - I love VueJS');
    }else {
        console.log(b + ' - I love programming');
    }
}

console.log("=========")

for (var c = 10; c <= 10; c++) {
    if (c % 2 == 0) {
        console.log(c + ' - I love programming');
    }else if (c % 3 == 0) {
        console.log(c + ' - I love VueJS');
    }else {
        console.log(c + ' - I love Javascript');
    }
}